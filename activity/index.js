// Initialize variable number
let number = Number(prompt("Provide a number:"));
console.log(`The number you provided is ${number}.`);


for(let numloop = number; numloop >= 0; numloop--) {
	console.log(numloop);
	// terminates the loop if numloop is less than or equal to 50
	if(numloop <= 50) {
		console.log("The current value is less than or equal to 50. Terminating the loop...");
		break;
	}
	// displays the message if numloop is divisible by 10
	if(numloop % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number...");
		continue;
	}
	// displays the number if numloop is divisible by 5
	if(numloop % 5 == 0) {
		console.log(numloop);
		continue;
	};
}; //end of for loop


// Initialize variables
let word = "supercalifragilisticexpialidocious";
let consonant = "";
let vowel = "";

// string iteration
for(let i = 0; i < word.length; i++) {
	console.log(word[i]);
};


for(let i = 0; i < word.length; i++) {
	// checks whether the word has vowels and will just continue on the iteration
	if(
		word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u"
	) {
		vowel += word[i];
		continue;
	}

	else {
		consonant += word[i];
	}
}; // end of for loop


console.log("Consonants of the word")
console.log(consonant);

// consonants iteration
for(i = 0; i < consonant.length; i++) {
	console.log(consonant[i]);
};


console.log("Vowels of the word")
console.log(vowel);

// vowels iteration
for(i = 0; i < vowel.length; i++) {
	console.log(vowel[i]);
};